package com.cola.colaboot.config.exception;

public class ColaException extends RuntimeException {
    public ColaException() {
    }

    public ColaException(String message) {
        super(message);
    }
}
