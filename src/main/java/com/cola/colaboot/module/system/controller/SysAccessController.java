package com.cola.colaboot.module.system.controller;

import com.cola.colaboot.config.dto.Res;
import com.cola.colaboot.module.system.pojo.SysAccess;
import com.cola.colaboot.module.system.service.SysAccessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sysAccess")
public class SysAccessController {

    @Autowired
    private SysAccessService accessService;

    /**
     * @param access    查询参数
     * @return  返回前角色有权限的子集，没有parentID则返回第一级
     */
    @GetMapping("/listRoleAccessByParent")
    public Res<?> listRoleAccessByParent(SysAccess access){
        return Res.ok(accessService.listRoleAccessByParent(access));
    }

    /**
     * @param menuType  1菜单 0所有
     * @return 返回当前角色有权限的树形结构菜单和方法
     */
    @GetMapping("/treeRoleAccess")
    public Res<?> treeRoleAccess(Integer menuType){
        return Res.ok(accessService.treeRoleAccess(menuType));
    }

    @GetMapping("/getAccessIdsByRole")
    public Res<?> getAccessIdsByRole(Integer roleId){
        return Res.ok(accessService.getAccessIdsByRole(roleId));
    }

    @PostMapping("/saveOrUpdate")
    public Res<?> saveOrUpdate(@RequestBody SysAccess access){
        accessService.doSave(access);
        return Res.ok();
    }

    @DeleteMapping("/delete")
    public Res<?> delete(String id){
        accessService.deleteNative(id);
        return Res.ok();
    }
}
