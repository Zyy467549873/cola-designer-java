放回包装类

``````
{
	"code":1,//状态码1正常,-1系统错误
	"msg":"",
	"data":Object
}
``````



### 1.登录

url:	/login

method: 	POST

body:

```json
{
    "username":"",
    "password":"",//base64加密
    "authcode":""//验证码
}
```



### 2.列表

url:	/design/pageList

method:	GET

param:

``````json
{
	"pageNo":1,//页面
	"pageSize":10,//页容量
	"title":""//大屏名称，搜索参数，非必传
}
``````



### 3.详情

url:	/design/getById/{id}/{mode}/{viewCode}

method:	GET

pathVariable:

| path       | 备注                    |
| ---------- | ----------------------- |
| {id}       | 大屏ID                  |
| {mode}     | 0编辑，1发布浏览，2预览 |
| {viewCode} | 访问码                  |

mode=1(正式浏览)时需访问码必传



### 4.检验访问码

url:	/design/authViewCode

method:	GET

param:

``````
{
	"id":"",//大屏ID
	"viewCode":""//访问码
}
``````



### 5.保存

url:	/design/saveOrUpdate

method:	POST

body:

``````json
{
    "id":"",//为null时保存，不为null时更新
    "title":"",//大屏标题
    "simpleDesc":"",//大屏描述
    "bgImg":"",//背景图片地址
    "bgColor":,//背景颜色
    "scaleX":1920,//尺寸X
    "scaleY":1080,//尺寸Y
    "components":"",//前端组件JSON串
    "designImgPath":""//保存后生成的设计图
    "state":"",//禁用状态：1启用,2禁用,-1删除
    "viewCode":"",
}
``````





### 6.删除

url:	/design/delete

method:	DELETE

param:

``````json
{
    id:"大屏ID"
}
``````





### 7.图片资源分组

url:	/imgGroup/listAll

method:	GET





### 8.图片资源列表

url:	/imgPool/pageList

method:	GET

param:

``````
{
	"pageNo": 1,//页码
	"pageSize": 10,//页容量
	"groupId":""//分组ID非必传
}
``````

### 9.图片上传

### 10.保存图片资源

